/// <reference types="Cypress" />


context('Window', () => {

  beforeEach(() => {
    cy.visit("/")
  })

  it('Toolbar', () => {
    cy.get('.q-toolbar').should('exist');
    cy.get('.q-toolbar__title')
      .should('contain', 'TRAC')
    cy.get('.q-toolbar')
      .find('.q-btn')
      .find('.q-icon')
      .should('have.text','menu')
    cy.get('.q-toolbar')
      .find('.q-btn')
      .click
  })
})
