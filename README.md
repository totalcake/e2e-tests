# e2e-tests

## Project setup 

### Build cypress container in debian
```
./build.sh
```
For centos only
```
docker build centos/ -t cypress:centos
```
### Run your end-to-end tests
Add specs files in [app](/app/cypress/integration)
then
```
./run.sh
```
For centos only
```
docker run cypress:centos
```
Some [examples](/app/examples)
## Customize configuration
Configure base URL to test ( before build !!!)
[cypress.json](/app/cypress.json)


