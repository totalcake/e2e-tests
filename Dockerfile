FROM node:lts

RUN apt-get update && apt-get install -y \
    software-properties-common \
    unzip \
    curl \
    xvfb \
    libgtk-3-0 \
    libgtk-3-dev \
    libxss1 \
    libgconf2-4 \
    libnss3 \
    libasound2

ADD ./app /home/app/

WORKDIR /home/app/ 

RUN npm install

CMD [ "bash","entry.sh" ]







